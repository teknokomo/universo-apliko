import axios from 'axios'

axios.defaults.baseURL = 'https://erp.boozina.ru/api/v1.0/';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.timeout = 5000; // таймаут соединения

export default axios;
