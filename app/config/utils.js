export const graphQlUrlize = query => {
  if (query.hasOwnProperty("query")) {
    let params = "query=" + encodeURIComponent(query.query);
    params += query.hasOwnProperty("variables")
      ? "&variables=" + encodeURIComponent(JSON.stringify(query.variables))
      : "";

    return "?" + params;
  }
  return "";
};
