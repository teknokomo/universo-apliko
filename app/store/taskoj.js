import Vue from "vue";
import { graphQlUrlize } from "../config/utils";
import axios from "../config/axios";
import headers from "../config/auth_data";

export default {
  namespaced: true,
  state() {
    return {
      taskoj: {}
    };
  },

  getters: {
    taskoj(state) {
      console.log("asdasdasd");
      console.log(JSON.stringify(state.taskoj));

      return state.taskoj;
    }
  },

  mutations: {
    setTaskoj(state, data) {
      state.taskoj = data.edges;
    },

    clear(state) {
      state.taskoj = {
        pageInfo: {
          hasNextPage: false,
          startCursor: "",
          endCursor: ""
        },
        edges: {}
      };
    }
  },

  actions: {
    clearTaskoj(store) {
      store.commit("clear");
    },

    loadTaskoj(store, count = 25) {
      let query = `
query($first: Int!, $after: String) {
  xeneralajTaskoj(first: $first, after: $after, orderBy: ["publikiga_dato"]) {
    pageInfo {
      hasNextPage
      startCursor
      endCursor
    }
    edges {
      node {
        objId
        uuid
        autoroLaboraRolo {
          speciala
          nomo {
            enhavo
          }
        }
        autoro {
          objId
          uuid
          unuaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
        }
        priskribo {
          enhavo
        }
      }
    }
  }
}
      `;

      let variables = {
        first: count
      };

      return axios
        .get(graphQlUrlize({ query: query, variables: variables }), headers.get)
        .then(response => {
          store.commit("setTaskoj", response.data.data.xeneralajTaskoj);
          return Promise.resolve();
        })
        .catch(e => {
          return Promise.reject(e);
        });
    }
  }
};
