import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import auth from "./auth";
import taskoj from "./taskoj";

export default new Vuex.Store({
  modules: {
    auth,
    taskoj
  }
});
